# Logger
unicode_plotting = enable
projection_axis = z
pseudocolor_layer = 1
colormap = rainbow

# Report
report_pseudocolor = rho
report_pseudocolor  = e_tot
report_pseudocolor   = temp
report_pseudocolor    = ntr_frac_0
report_frequency = 10

# Initial Condition
ic_type = simple
ic_grid = 128 128 128
ic_box_lengths = 0.8 0.8 0.8 'kpc'
ic_redshift = 0.0

# AMR
ic_nlevels = 1
max_nboxes = 1

# Internal Units
density_unit = "m_H / cm^3"
length_unit = "Mpc"
time_unit = "Myr"

# Boundary Conditions
left_bc = reflective
right_bc = outflow
bottom_bc = reflective
top_bc = outflow
back_bc = reflective
front_bc = outflow

# Thermodynamics
ideal_gas_type = monatomic

# CFL
courant_number = 0.8

# Exact Riemann Solver
vacuum_pressure = 0d0  # cm^-3 Mpc^2 Myr^-2
vacuum_density = 0d0  # cm^-3
tolerance = 1.d-8
n_iteration = 10000

# Slope Limiter
slope_limiter = van_leer
slope_limiter_omega = 0d0

# Chemistry
elements = H He
element_abundances = 1.0 0.0

# Ionisation Equilibrium
uvb_equilibrium = disable
uvb_self_shielding = disable
collisional_ionization_equilibrium = disable
photoionization_equilibrium = disable
species_cases = case_b case_b case_b

# MUSCL-Hancock solver
solver_type = memory_intensive


# Drawing
canvas = density_power_law 0 0 0 14.687999999999999 14.687999999999999 -2.0 3.2 3.2 0d0 0d0 0d0 2.76275466e-12 100 1 0 0

# Sanity Check
sanity_check = disable

# Stabilizer
stabilizer = disable

# Chombo Output
chombo_prefix = "./Iliev-6-CaseB-1000"
chombo_nickname = 'Iliev-6-CaseB'
chombo_output_restart_backup_every = 10
chombo_output_rule  = linear 0.0 75 76
chombo_output_final_time = 75

# species
NSpecies = 1

# general
ActualRun = .true.
VersionID = iliev-6-CaseB

# initial evolution
DoInitEvol = .false.
InitCollEq = .false.

# light speed params
LightSpeedLimit = .false.

# Recombination
CaseA = .false.
ClumpingFactor = 1

# UVB
InitUVBEq = .false.
UVB_SS = .false.
KeepBKG = .false.

# log
Verbosity = 3

# timestep
TimeStepFact = 0.1
InitialSimTime = 0.0
SimulationTime = 75 # Myr

# cosmology
OmegaBaryonNow = 0.0486
OmegaCDMNow = 0.3075
OmegaLambdaNow = 0.6910098315260953
HubbleConstNow = 67.74  # km / s / Mpc
InitialRedshift = 0.0

# ic
InitHIIradius = 0.0390625  # box size, 5px
SafetyDist = 0.0078125  # box size, 1px

# Heating & Cooling mechanisms
IncludeCollIon = 1
IncludeCollExc = 1
IncludeHubbleCool = 0
IncludeComptCool = 0
IncludeQSOComptHeat = .false.

# Single source
NSources = 1
SourceCoords = 0.0078125 0.0078125 0.0078125
SpectrumType = 2  # Blackbody
BlackBodyTemp = 1d5
TotalIonRate = 50d0
OpAngle = 0.0
NSpectralRegions = 3
NSpectralBins = 10 10 50
MinMaxEnergy = 1 1.8 1.8 4 4 100

# Ray tracing
NRays = 5
MaxTau = 50 50 50
